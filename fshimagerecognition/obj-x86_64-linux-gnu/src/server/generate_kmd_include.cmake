  execute_process (COMMAND /usr/bin/kurento-module-creator -r /home/thomas/fsh-image-recognition/fshimagerecognition/src/server/interface ;-dr;/usr/share/kurento/modules -o /home/thomas/fsh-image-recognition/fshimagerecognition/obj-x86_64-linux-gnu/src/server/)

  file (READ /home/thomas/fsh-image-recognition/fshimagerecognition/obj-x86_64-linux-gnu/src/server/fshimagerecognition.kmd.json KMD_DATA)

  string (REGEX REPLACE "\n *" "" KMD_DATA ${KMD_DATA})
  string (REPLACE "\"" "\\\"" KMD_DATA ${KMD_DATA})
  string (REPLACE "\\n" "\\\\n" KMD_DATA ${KMD_DATA})
  set (KMD_DATA "\"${KMD_DATA}\"")

  file (WRITE /home/thomas/fsh-image-recognition/fshimagerecognition/obj-x86_64-linux-gnu/src/server/fshimagerecognition.kmd.json ${KMD_DATA})
