#ifndef __FSHIMAGERECOGNITION_CONFIG_H__
#define __FSHIMAGERECOGNITION_CONFIG_H__

/* Version */
#define VERSION "0.0.1~.g"

/* Package name */
#define PACKAGE "fshimagerecognition"

/* The gettext domain name */
#define GETTEXT_PACKAGE "fshimagerecognition"

/* Library installation directory */
#define KURENTO_MODULES_SO_DIR "/usr/lib/x86_64-linux-gnu/kurento/modules"

#endif /* __FSHIMAGERECOGNITION_CONFIG_H__ */
